package scraper

import (
	"database/sql"
	"fmt"
	"log"
	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

func InitDB() {
	var err error
	db, err = sql.Open("mysql", "root:@tcp(127.0.0.1:3306)/scraper")
	if err != nil {
	  log.Fatal(err)
	}

	dropTable := `DROP TABLE IF EXISTS blogs;`
	_, err = db.Exec(dropTable)
	if err != nil {
	  log.Fatal(err)
	}

	createTable :=
	`CREATE TABLE IF NOT EXISTS blogs (
		id INT AUTO_INCREMENT PRIMARY KEY,
		title VARCHAR(255) NOT NULL,
		date VARCHAR(255) NOT NULL,
		author VARCHAR(255) NOT NULL
	);`

	_, err = db.Exec(createTable)
	if err != nil {
	  log.Fatal(err)
	}
}

func SaveBlog(blog Blog) {
	_, err := db.Exec("INSERT INTO blogs (title, date, author) VALUES (?, ?, ?)", blog.Title, blog.Date, blog.Author)
	if err != nil {
	  fmt.Println("Error inserting new blog: ", err)
	}
}