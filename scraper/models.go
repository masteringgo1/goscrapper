package scraper

type Blog struct {
	Title string
	Date string
	Author string
}